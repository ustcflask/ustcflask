from .qa import bp as qa_bp
from .user import bp as user_bp
from .posts import bp as posts_bp
from .blog import bp as blog_bp