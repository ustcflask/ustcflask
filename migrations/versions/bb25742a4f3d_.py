"""empty message

Revision ID: bb25742a4f3d
Revises: e381bb2f5b78
Create Date: 2022-06-21 01:39:55.834279

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'bb25742a4f3d'
down_revision = 'e381bb2f5b78'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('question', sa.Column('content', sa.String(length=1200), nullable=False))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('question', 'content')
    # ### end Alembic commands ###
