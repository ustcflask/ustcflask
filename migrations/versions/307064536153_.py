"""empty message

Revision ID: 307064536153
Revises: c8775770a411
Create Date: 2022-07-04 20:21:08.703833

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '307064536153'
down_revision = 'c8775770a411'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('blogcollection', sa.Column('create_time', sa.DateTime(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('blogcollection', 'create_time')
    # ### end Alembic commands ###
