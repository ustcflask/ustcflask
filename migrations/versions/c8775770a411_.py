"""empty message

Revision ID: c8775770a411
Revises: 5c792b7189cf
Create Date: 2022-07-04 17:09:59.404055

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c8775770a411'
down_revision = '5c792b7189cf'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('blog', sa.Column('collects', sa.Integer(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('blog', 'collects')
    # ### end Alembic commands ###
