"""empty message

Revision ID: 65cbea46d524
Revises: 184eac01ed09
Create Date: 2022-06-20 11:57:34.705576

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '65cbea46d524'
down_revision = '184eac01ed09'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('answer', sa.Column('likes', sa.Integer(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('answer', 'likes')
    # ### end Alembic commands ###
